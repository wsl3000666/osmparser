function [color_code] = mapLabel2Color(label)

switch label
    case 0
        color_code = [1, 0, 0];
    case 1
        color_code = [0, 1, 0];
    case 2
        color_code = [0, 0, 1];
    case 3
        color_code = [1, 1, 0];
    case 4
        color_code = [1, 0, 1];
    case 5
        color_code = [0, 1, 1];
    otherwise
        error('Error Label.')
end

end