%% ==================== Basic Info ====================
%  Extract Intersection Information from OSM

%  Modification:
%  2015.06.02 (c) Wei-Chiu Ma (weichium@andrew.cmu.edu)
%  =====================================================
%% Code Starts
%clc;clear;
OSM_DIR = '/Users/percentage/Documents/CMU/Research/Geolocalization/parseOSM';
INTER_DIR = '/Users/percentage/Documents/CMU/Research/Geolocalization/parseOSM/intersection';
OUTPUT_DIR = '/Users/percentage/Documents/CMU/Research/Geolocalization/parseOSM/sequence/0000';
%mkdir(PARKING_DIR);

osm_list = dir(fullfile(OSM_DIR, '*.osm'));

for i = 1 : length(osm_list)
    %osm_filename = fullfile(OSM_DIR, sprintf('%04d.osm', i-1));
    %osm_filename = fullfile(OSM_DIR, 'map01.osm');
    fprintf('%04d: ', i-1);
    tic;
    %[parsed_osm, osm_xml] = parse_openstreetmap(osm_filename);
    fig = figure(2);
    clf(fig);
    ax = axes('Parent', fig);
    hold(ax, 'on')
    %plot_way(ax, parsed_osm);
    [buildings, roads, parkings, intersections, inter_matrix, bounds, nd2direction, origin_pos] = plot_way_slwang(ax, parsed_osm, 'meters', [8.437134430837500; 49.009347760599000]);
    time = toc;
    %axis equal
    fprintf('%2.4f seconds \n', time);
%     save(fullfile(OUTPUT_DIR, 'parkings.mat'), 'parkings');
%     save(fullfile(OUTPUT_DIR, 'buildings.mat'), 'buildings');
%     save(fullfile(OUTPUT_DIR, 'roads.mat'), 'roads');
%     save(fullfile(OUTPUT_DIR, 'intersections.mat'), 'intersections');
    pause(0.01);
    break;
end
