function [inter_label, inter_type, inter_dist] = projSeq2Map(seqs, seq_num, inter_matrix, nd2direction, nd_way_direction, version, origin_pos, latitude_scale_factor, k, PROJ_DIR)

if ~exist('seq_num')
    seq_num = [1: size(seqs, 1)];
end

% temp
if ~exist('latitude_scale_factor')    
    latitude_scale_factor = 0.6559;
end

if ~exist('version')
    error('Please specify which unit you are using [e.g. meters, lat/lon...etc]');
end

if isempty(inter_matrix)
    inter_matrix = zeros(1, 11);
end

if strcmp(version, 'meters')
    radius = 23;
else
    radius = 0.00025;
end

for i = seq_num
    seq = seqs{i, 1};
    sample = [1:1:size(seq, 1)];
    [inter_label, inter_type, inter_dist] = getIntersectionLabel(inter_matrix, nd2direction, nd_way_direction, version, seq(sample, [2 1]), seq(sample, 3), radius, origin_pos, latitude_scale_factor);
    if ~exist('k')
        plotSector(seq(sample, [2 1]), seq(sample, 3), radius, latitude_scale_factor, deg2rad(135/2), inter_label, inter_type, version, origin_pos);
    else
        %plotSector(seq(sample, [2 1]), seq(sample, 3), radius, latitude_scale_factor, deg2rad(90/2), inter_label, inter_type, version, origin_pos, k, PROJ_DIR);
    end
    %plotSector(seq(sample, [2 1]), seq(sample, 3), radius, latitude_scale_factor);
end


end

function [inter_label, inter_type, inter_dist] = getIntersectionLabel(inter_matrix, nd2direction, nd_way_direction, version, ori, angle, radius, origin_pos, latitude_scale_factor, fov)

if strcmp(version, 'meters')
    CLOSET_VIEWPOINT = 4; % To be more accurate, it should be 6.35. But we use a smaller value to tolerate the GPS noise.
else
    CLOSET_VIEWPOINT = 0.00001;
end

if ~exist('fov')
    fov = deg2rad(90/2);
end

if ~exist('version')
    error('Please specify which unit you are using [e.g. meters, lat/lon...etc]');
end

inter_label = zeros(size(ori, 1), 15);
inter_type = zeros(size(ori, 1), 15);
inter_dist = ones(size(ori, 1), 15)*100;

for i = 1:size(ori, 1)
    theta = angle(i, 1);
    
    if strcmp(version, 'meters')
        [x0, y0] = calPos(ori(i, :)', origin_pos);
        [x0, y0] = gps2camera(x0, y0, theta);
        x1 = x0 + radius * cos(theta-fov);
        y1 = y0 + radius * sin(theta-fov);    
        x2 = x0 + radius * cos(theta+fov);
        y2 = y0 + radius * sin(theta+fov);  
    
        X = [x0 x1 x2];
        Y = [y0 y1 y2];    

        %cand = (inter_matrix(:, 4) > min(X)) & (inter_matrix(:, 4) < max(X)) &...
        %     (inter_matrix(:, 5) > min(Y)) & (inter_matrix(:, 5) < max(Y));   
        cand = (inter_matrix(:, 4) > x0 - radius - 2) & (inter_matrix(:, 4) < x0 + radius + 2) &...
             (inter_matrix(:, 5) > y0 - radius - 2) & (inter_matrix(:, 5) < y0 + radius + 2); 
    else
        x0 = ori(i, 1);
        y0 = ori(i, 2);        
        x1 = x0 + radius * cos(theta-fov);
        y1 = y0 + radius * sin(theta-fov) * latitude_scale_factor;    
        x2 = x0 + radius * cos(theta+fov);
        y2 = y0 + radius * sin(theta+fov) * latitude_scale_factor; 
    
        X = [x0 x1 x2];
        Y = [y0 y1 y2];    

        cand = (inter_matrix(:, 2) > min(X)) & (inter_matrix(:, 2) < max(X)) &...
             (inter_matrix(:, 3) > min(Y)) & (inter_matrix(:, 3) < max(Y));        
    end
       

    cand = inter_matrix(cand, :);
    inter_ind = 1;
    for j = 1:size(cand, 1)
        if strcmp(version, 'meters')
            [fin, distance] = calRoadDirection(ori(i, :)', cand(j, [2 3])',latitude_scale_factor, 'meters', 'r');
        else
            dxy = diff([x0 y0; cand(j, [2 3])]); % 1*2
            fin = calRoadDirection([x0;y0], cand(j, [2 3])',latitude_scale_factor, 'latlon', 'r');
            distance = norm(dxy, 2);
        end
        if fin < theta+fov && fin > theta-fov            
            if distance < radius && distance > CLOSET_VIEWPOINT
                dir = nd2direction(cand(j, 1));
                dir = out2inDirection(dir);
                [d, inner_product] = findClosestDirection(fin, deg2rad(dir));
                onSR = onSameRoad1(cand(j, 1), inner_product, distance, nd_way_direction);
                if onSR
                    inter_label(i, inter_ind) = cand(j, 6);                                
                    if cand(j, 6) == 3 && cand(j, 7) ~=0
                        inter_type(i, inter_ind) = cand(j, 6+d);
                    end
                    inter_dist(i, inter_ind) = distance;
                    inter_ind = inter_ind + 1;
                end
            end            
        end
    end
    [inter_dist(i, :), dist_ind] = sort(inter_dist(i, :));
    %if i == 22
    %    i
    %end
    fprintf('sequence %04d...', i);
    inter_label(i, :) = inter_label(i, dist_ind);
    inter_type(i, :) = inter_type(i, dist_ind);
    
        
end
    
end

function [] = plotSector(ori, angle, radius, latitude_scale_factor, fov, inter_label, inter_type, version, origin_pos, k, OUT_DIR)

if ~exist('fov')
    fov = deg2rad(60);
end

if ~exist('version')
    error('Please specify which unit you are using [e.g. meters, lat/lon...etc]');
end

if ~exist('inter_label')
    inter_label = [];
end

for i = 1:size(ori, 1)
    theta = angle(i, 1);
    t = linspace(theta-fov, theta+fov);
    
    if strcmp(version, 'meters')
        [x0, y0] = calPos(ori(i, :)', origin_pos);
        plot(x0, y0, 'rx')
        [x0, y0] = gps2camera(x0, y0, theta);
        x1 = x0 + radius * cos(t);
        y1 = y0 + radius * sin(t);       
        t0 = plot([x0 x0+radius*cos(theta)], [y0 y0+radius*sin(theta)], 'y-');
    else
        x0 = ori(i, 1);
        y0 = ori(i, 2);        
        x1 = x0 + radius * cos(t);
        y1 = y0 + radius * sin(t) * latitude_scale_factor;          
        t0 = plot([x0 x0+radius*cos(theta)], [y0 y0+radius*sin(theta)*latitude_scale_factor], 'y-');
    end
    X = [x0 x1 x0];
    Y = [y0 y1 y0];     
   
    t1 = plot([x0 x1 x0], [y0 y1 y0], 'r-');    
    %plot([x0], [y0], 'x');
    t2 = text(x0, y0, num2str([1+(i-1)*10 inter_label(i, 1:3) inter_type(i, 1:3)]));
    
    saveas(gcf, fullfile(OUT_DIR, sprintf('%06d.png', k+i-1)));
    %set(t0,'Visible','off')
    delete(t0);
    delete(t1);
    delete(t2);
    
end
end

function [in_dir] = out2inDirection(out_dir)

dir_num = size(out_dir, 2);
in_dir = zeros(1, dir_num);
for i = 1:dir_num
    if out_dir(i) < 0
        in_dir(i) = out_dir(i) + 180;        
    else
        in_dir(i) = out_dir(i) - 180;
    end
end

end

function [closest_ind, inner_product] = findClosestDirection(car_dir, inter)

% c_ind = (car_dir < 0);
% i_ind = (inter < 0);
% 
% car_dir(c_ind) = car_dir(c_ind) + 2*pi;
% inter(i_ind) = inter(i_ind) + 2*pi;
% 
% [~, ind]= min(abs(inter - car_dir));


c_vec = [cos(car_dir), sin(car_dir)];
inter = [cos(inter)' sin(inter)'];

c_vec = repmat(c_vec, [size(inter, 1) 1]);
inner_product = sum(c_vec .* inter, 2);
[~, ind] = max(inner_product);

closest_ind = ind(1);

end

function [xp, yp] = gps2camera(x, y, fin)

r1 = 0.32;
r2 = 0.81;

xp = x + r1 * sin(fin) + r2 * cos(fin);
yp = y - r1 * cos(fin) + r2 * sin(fin);

end

function [same] = onSameRoad1(inter_id, inner_product, dist, nd_way_direction)

dist_threshold = 15;

way_direction = nd_way_direction(inter_id);

for i = 1:size(way_direction, 2)
    switch way_direction(1, i)
        case 0
        case 1
            inner_product(i, 1) = inner_product(i, 1) * (-1);
        case 2
            inner_product(i, 1) = abs(inner_product(i, 1));
        otherwise
            error('wrong way direction type')
    end    
end

if max(inner_product) < 0.5 && dist > dist_threshold
    same = false;
else
    same = true;
end

end

