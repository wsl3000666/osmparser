function [closest_ind] = findClosestDirection(car_dir, inter)

if sum(abs(car_dir) > 10) ~= 0 || sum(abs(inter) > 10) ~= 0
    error('Make sure the unit is radius.')
end

% dir_num = size(car_dir, 2);
% c_ind = (car_dir < 0);
% i_ind = (inter < 0);
% 
% car_dir(c_ind) = car_dir(c_ind) + 2*pi;
% inter(i_ind) = inter(i_ind) + 2*pi;
% 
% [~, ind]= min(abs(inter - car_dir));

c_vec = [cos(car_dir), sin(car_dir)];
inter = [cos(inter)' sin(inter)'];

c_vec = repmat(c_vec, [size(inter, 1) 1]);
[~, ind] = max(sum(c_vec .* inter, 2));

closest_ind = ind(1);

end