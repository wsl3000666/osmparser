function [] = getOSM4KITTI()

DATA_FOLDER = '/ais/gobi3/u/slwang/kitti3d/code/sun_dir/data';

f_osm = fopen('getOSM4KITTI.sh', 'wt+');
meta_list = dir(fullfile(DATA_FOLDER, '*.mat'));

for i = 1:length(meta_list)
    file_name = meta_list(i, 1).name;
    load(fullfile(DATA_FOLDER, file_name));
    [lt_lat, lt_lon, rb_lat, rb_lon] = getLatLon4KITTI(metadata);
    fprintf(f_osm, 'wget -O map%04d.osm http://api.openstreetmap.org/api/0.6/map?bbox=%2.12f,%2.12f,%2.12f,%2.12f\n', i, lt_lon, lt_lat, rb_lon, rb_lat);
    fprintf('processing %s...\n', file_name);
end

fclose(f_osm);

end

function [lt_lat, lt_lon, rb_lat, rb_lon] = getLatLon4KITTI(metadata)

img_num = size(metadata, 2);
oxt = zeros(img_num, 2);

for i = 1:img_num
    o = metadata(1, i).oxts;
    oxt(i, 1:2) = o(1, 1:2); % latitude, longitude    
end

sort_lat = sort(oxt(:, 1));
sort_lon = sort(oxt(:, 2));

diff_lat = abs(sort_lat(1, 1) - sort_lat(end, 1));
diff_lon = abs(sort_lon(1, 1) - sort_lon(end, 1));



lt_lat = sort_lat(1, 1) - 0.2 * diff_lat;
lt_lon = sort_lon(1, 1) - 0.2 * diff_lon;

rb_lat = sort_lat(end, 1) + 0.2 * diff_lat;
rb_lon = sort_lon(end, 1) + 0.2 * diff_lon;

end