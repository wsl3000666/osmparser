function [distribution] = randSampleSunValitdation()

VAL_DIR = '/u/slwang/kitti3d/code/sun_dir';
N = 200;

f_test = fopen(fullfile(VAL_DIR, 'val.txt'));
C = textscan(f_test, '%s %d');
fclose(f_test);

img_num = size(C{1, 1}, 1);
img_list = C{1, 1};
gt_label = double(C{1, 2});

rand = randperm(img_num);
rand = rand(1:N);

distribution = zeros(1, 4);

f_human = fopen(fullfile(VAL_DIR, 'human_val.txt'), 'wt');
for i = 1:N
    fprintf(f_human, '%s %d\n', img_list{rand(i), 1}, gt_label(rand(i), 1));
    distribution(1, gt_label(rand(i), 1)+1) = distribution(1, gt_label(rand(i), 1)+1) + 1;
end
fclose(f_human);

end