function [r] = deg2rad(d)

r = d/360*2*pi;

end