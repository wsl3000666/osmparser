function [building_out, road_out, parking_out, else_out] = plot_way_apt(ax, parsed_osm, map_img_filename)
%PLOT_WAY   plot parsed OpenStreetMap file
%
% usage
%   PLOT_WAY(ax, parsed_osm)
%
% input
%   ax = axes object handle
%   parsed_osm = parsed OpenStreetMap (.osm) XML file,
%                as returned by function parse_openstreetmap
%   map_img_filename = map image filename to load and plot under the
%                      transportation network
%                    = string (optional)
%
% 2010.11.06 (c) Ioannis Filippidis, jfilippidis@gmail.com
%
% See also PARSE_OPENSTREETMAP, EXTRACT_CONNECTIVITY.

% ToDo
%   add double way roads

if nargin < 3
    map_img_filename = [];
end

[bounds, node, way, ~] = assign_from_parsed(parsed_osm);

disp_info(bounds, size(node.id, 2), size(way.id, 2))
[building_out, road_out, parking_out, else_out] = show_ways(ax, bounds, node, way, map_img_filename);

function [building_out, road_out, parking_out, else_out] = show_ways(hax, bounds, node, way, map_img_filename)
% show_map(hax, bounds, map_img_filename);

%plot(node.xy(1,:), node.xy(2,:), '.')
building_out = {};
road_out = {};
parking_out = {};
else_out = {};
building_ind = 1;
road_ind = 1;
else_ind = 1;

key_catalog = {};
for i=1:size(way.id, 2)
    [keys, ~] = get_all_tag_key(way.tag{1,i} );
    flag = 0;
    for k = 1 : length(keys)
    key = keys{k};
    if (~isempty(strfind(key, 'building')))
        flag = 2; break;
    elseif (~isempty(strfind(key, 'highway')))
        flag = 1; break;
    end
    end
    
    % plot highway
    way_nd_ids = way.nd{1, i};
    num_nd = size(way_nd_ids, 2);
    nd_coor = zeros(2, num_nd);
    nd_ids = node.id;
    for j=1:num_nd
        cur_nd_id = way_nd_ids(1, j);
        if ~isempty(node.xy(:, cur_nd_id == nd_ids))
             nd_coor(:, j) = node.xy(:, cur_nd_id == nd_ids);
        end
    end
    
    % remove zeros
    nd_coor(any(nd_coor==0,2),:)=[];
    
    if ~isempty(nd_coor)
        % plot way (highway = blue, other = green)
        if flag == 1
            % plot(hax, nd_coor(1,:), nd_coor(2,:), 'b-')
            road_out{road_ind} = nd_coor;
            road_ind = road_ind + 1;
        elseif flag == 2
            building_out{building_ind} = nd_coor;
            building_ind = building_ind + 1;
            % plot(hax, nd_coor(1,:), nd_coor(2,:), 'r-')
        elseif flag == 3
            else_out{else_ind} = nd_coor;
            else_ind = else_ind + 1;
            % plot(hax, nd_coor(1,:), nd_coor(2,:), 'k-');
        else
            % plot(hax, nd_coor(1,:), nd_coor(2,:), 'g--');
            else_out{else_ind} = nd_coor;
            else_ind = else_ind + 1;
        end
    end
    
    %waitforbuttonpress
end
disp(key_catalog.')

function [] = disp_info(bounds, Nnode, Nway)
disp( ['Bounds: xmin = ' num2str(bounds(1,1)),...
    ', xmax = ', num2str(bounds(1,2)),...
    ', ymin = ', num2str(bounds(2,1)),...
    ', ymax = ', num2str(bounds(2,2)) ] )
disp( ['Number of nodes: ' num2str(Nnode)] )
disp( ['Number of ways: ' num2str(Nway)] )
