function [key, val] = get_all_tag_key(tag)
% get tags and key values for ways
%
% 2010.11.21 (c) Ioannis Filippidis, jfilippidis@gmail.com
%
% See also PLOT_WAY, EXTRACT_CONNECTIVITY.

if isstruct(tag) == 1
    key{1} = tag.Attributes.k;
    val{1} = tag.Attributes.v;
elseif iscell(tag) == 1
    key = cell(0);
    for i = 1 : length(tag)
        key{i} = tag{i}.Attributes.k;
        val{i} = tag{i}.Attributes.v;
    end
else
    if isempty(tag)
        warning('Way has NO tag.')
    else
        warning('Way has tag which is not a structure nor cell array, but:')
        disp(tag)
    end
    
    key = '';
    val = '';
end
