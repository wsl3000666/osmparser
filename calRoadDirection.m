function [direction, distance] = calRoadDirection(p1, p2, latitude_scale_factor, version, rord)

if ~exist('rord')
    rord = 'd';
end

if ~exist('version')
    error('Please specify the unit you are usingx [e.g. meters, lat/lon...etc]');
end

xy_diff = zeros(2, 1);
switch version
    case 'meters'
        [xy_diff(1, 1), xy_diff(2, 1)]  = calPos(p2(2, 1), p2(1, 1), p1(2, 1), p1(1, 1));
        direction = atand(xy_diff(2, 1)/xy_diff(1, 1));
        distance = norm(xy_diff);
    case 'latlon'
        xy_diff = p2 - p1;
        direction = atand(xy_diff(2, 1)/(xy_diff(1, 1)* latitude_scale_factor));
        distance = 0;
    otherwise
        error('Please specify a supportable unit');
end

%xy_diff = p2 - p1;
%xy_diff(2, 1) = xy_diff(2, 1) * latitude_scale_factor;
%direction = atand(xy_diff(2, 1)/(xy_diff(1, 1)* latitude_scale_factor));

if direction > 0 && xy_diff(2, 1) < 0
    direction = direction - 180;
elseif direction < 0 && xy_diff(2, 1) > 0
    direction = direction + 180;
end

if rord == 'r'
    direction = deg2rad(direction);
end

end
