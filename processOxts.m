function [pos] = processOxts(oxts)

file_num = size(oxts, 1);
pos = cell(file_num, 1);

for i = 1:file_num
    oxt = oxts{i, 1};
    pos{i, 1} = oxt(:, [1 2 6]);    
end


end