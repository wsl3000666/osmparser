function [new_inter_type] = mapInterType2to1(inter_label, inter_type)
% label + type --> label
%   0   +   0  -->   0
%   3   +   0  -->   1
%   3   +   1  -->   2
%   3   +   2  -->   3
%   3   +   3  -->   4
%   4   +   0  -->   5
%   2   +   0  -->   6

seq_len = size(inter_label, 1);
new_inter_type = zeros(seq_len, 1);

for i = 1:seq_len
    switch inter_label(i, 1)
        case 0
            new_inter_type(i, 1) = 0;
%         case 2
%             new_inter_type(i, 1) = 6;
        case 4
            new_inter_type(i, 1) = 5;
        case 3
            switch inter_type(i, 1)
                case 0
                    new_inter_type(i, 1) = 1;
                case 1
                    new_inter_type(i, 1) = 2;
                case 2
                    new_inter_type(i, 1) = 3;
                case 3
                    new_inter_type(i, 1) = 4;
                otherwise
                    error('Invalid inter_type.');
            end
        otherwise
            error('Invalid inter_label.');
    end
end

end