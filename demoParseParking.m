%clc;clear;
OSM_DIR = '/Users/percentage/Documents/CMU/Research/Geolocalization/parseOSM';
PARKING_DIR = '/Users/percentage/Documents/CMU/Research/Geolocalization/parseOSM/building';
%mkdir(PARKING_DIR);

osm_list = dir(fullfile(OSM_DIR, '*.osm'));

for i = 1 : length(osm_list)
    %osm_filename = fullfile(OSM_DIR, sprintf('%04d.osm', i-1));
    osm_filename = fullfile(OSM_DIR, 'small_map.osm');
    fprintf('%04d: ', i-1);
    tic;
    %[parsed_osm, osm_xml] = parse_openstreetmap(osm_filename);
    fig = figure(2);
    clf(fig);
    ax = axes('Parent', fig);
    hold(ax, 'on')
    %plot_way(ax, parsed_osm);
    [buildings, roads, parkings] = plot_way_slwang(ax, parsed_osm);
    time = toc;
    fprintf('%2.4f seconds \n', time);
    %road_filename = fullfile(PARKING_DIR, sprintf('%04d.mat', i-1));
    %save(road_filename, 'parkings');
    pause(0.01);
end