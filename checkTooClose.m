function [similar] = checkTooClose(oxt1, oxt2)

pos = [oxt1(1, [2 1]); oxt2(1, [2 1])]; % [x y]
angle = [oxt1(3); oxt2(3)];

[dx, dz] = calPos(pos(2, :), pos(1, :));
theta = innerProduct(angle(2), angle(1));
dist = sqrt(dx^2 + dz^2);

if dist < 5 && theta < 20
    similar = true;
else
    similar = false;
end


end


function [theta] = innerProduct(t1, t2)

v1 = [cos(t1) sin(t1)];
v2 = [cos(t2) sin(t2)];

theta = acosd(sum(v1 .* v2, 2));

end
