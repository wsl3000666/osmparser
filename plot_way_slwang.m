function [building_out, road_out, parking_out, intersection_out, inter_matrix, bounds, nd2direction, nd_way_direction, origin_pos, temp_tags] = plot_way_slwang(ax, parsed_osm, version, temp_tags, origin_pos, map_img_filename)
%PLOT_WAY   plot parsed OpenStreetMap file
%
% usage
%   PLOT_WAY(ax, parsed_osm)
%
% input
%   ax = axes object handle
%   parsed_osm = parsed OpenStreetMap (.osm) XML file,
%                as returned by function parse_openstreetmap
%   map_img_filename = map image filename to load and plot under the
%                      transportation network
%                    = string (optional)
%
% 2010.11.06 (c) Ioannis Filippidis, jfilippidis@gmail.com
%
% See also PARSE_OPENSTREETMAP, EXTRACT_CONNECTIVITY.

% ToDo
%   add double way roads
%
% Modified
% 2015.06.02, Wei-Chiu Ma, weichium@andrew.cmu.edu
origin_pos = [0, 0];
version = 'meters';
temp_tags = cell(0);
if ~exist('map_img_filename')
    map_img_filename = [];
end

if ~exist('origin_pos')
    origin_pos = zeros(2, 1);
end

if ~exist('version')
    error('Please specify which unit you are using [e.g. meters, lat/lon...etc]');
end

% way: 1*1 struct with three fields --> id, nd, tag
[bounds, node, way, ~] = assign_from_parsed(parsed_osm);

disp_info(bounds, size(node.id, 2), size(way.id, 2))
[building_out, road_out, parking_out, intersection_out, inter_matrix, nd2direction, nd_way_direction, origin_pos, temp_tags] = show_ways(ax, bounds, version, node, way, origin_pos, temp_tags, map_img_filename);

function [building_out, road_out, parking_out, intersection_out, inter_matrix, nd2direction, nd_way_direction, origin_pos, temp_tags] = show_ways(hax, bounds, version, node, way,  origin_pos, temp_tags, map_img_filename)

if sum(origin_pos == 0) == 2
    origin_pos = mean(bounds, 2); % [long; lat]
end

show_map(hax, bounds, version, origin_pos, map_img_filename);
latitide_scale_factor = cosd(mean(bounds(2, :)));


%plot(node.xy(1,:), node.xy(2,:), '.')
building_out = {};
road_out = {};
parking_out = {};
intersection_out = {};
building_ind = 1;
road_ind = 1;
parking_ind = 1;
intersection_ind = 1;

% [Wei-Chiu] Parameters for finding nodes representing intersections
possible_intersect = false;
nd2way = containers.Map('KeyType', 'double', 'ValueType', 'any');
nd2coord = containers.Map('KeyType', 'double', 'ValueType', 'any');
nd2coord_m = containers.Map('KeyType', 'double', 'ValueType', 'any');
intersection_out = {}; 
intersection_ind = 1;
inter_matrix = [];
nd2direction = containers.Map('KeyType', 'double', 'ValueType', 'any'); % save out direction (in degrees)
nd_way_direction = containers.Map('KeyType', 'double', 'ValueType', 'any'); % out direction consistent with road direction or not (0: reverse, 1:same, 2:both direction)
way_oneway = containers.Map('KeyType', 'double', 'ValueType', 'any');
% Tag of interests
TOI = {'highway', 'amenity', 'building', 'railway', 'addr:city', 'maxspeed', 'bicycle', 'leisure', 'landuse'};

key_catalog = {};
for i=1:size(way.id, 2)
    possible_intersect = false;
    [key, val] = get_way_tag_key(way.tag{1,i});    
    
    if way.id(1, i) == 137382172
        i
    end
    
%     if way.id(1, i) == 354085928
%         way.id(1, i)
%     end    
    
    % [Wei-Chiu] Extract TOI from returned tags
    flag = 0;
    k = '';
    v = '';
    for t = 1:size(TOI, 2)
        ind = find(ismember(key, TOI{1, t}));
        if ~isempty(ind)
            k = TOI{1, t};
            v = val{1, ind};
            break
        end
    end

    one_ind = find(ismember(key, 'oneway'));
    if ~isempty(one_ind)
        if strcmp(val{1, one_ind}, 'yes') == 1
            way_oneway(way.id(1, i)) = 1;
        else
            way_oneway(way.id(1, i)) = 0;
        end
    else
        way_oneway(way.id(1, i)) = 0;
    end
    
    
    % find unique way types
    if isempty(k)
        %
    elseif isempty( find(ismember(key_catalog, k) == 1, 1) )
        key_catalog(1, end+1) = {k};
    end
            
    % way = highway or amenity ?    
    switch k
        case 'highway'
            %flag = 1; 
            % bus stop ?
            if strcmp(val, 'bus_stop')
                disp('Bus stop found')
            end
            
            if ~isempty(strfind(v, '_link'))
                v = v(1:strfind(v, '_link')-1);
            end
            
            % [Wei-Chiu]
            switch v
                case 'residential'
                    possible_intersect = true;      
                    flag = 1;
                case 'service'
                    if sum(ismember(key, 'name')) ~= 0
                        possible_intersect = true;
                    end                    
                    %possible_intersect = true;
                    flag = 1;
                case 'primary'
                    possible_intersect = true;
                    flag = 1;
                case 'secondary'
                    possible_intersect = true;
                    flag = 1;
                case 'secondary_link'
                    possible_intersect = true;
                    flag = 1;       
                case 'motorway'
                    possible_intersect = true;
                    flag = 1;     
                case 'motorway_link'
                    possible_intersect = true;
                    flag = 1;                    
                case 'tertiary'
                    possible_intersect = true;
                    flag = 1;
                case 'living_street'
                    possible_intersect = true;
                    flag = 1; 
                case 'trunk_link'
                    possible_intersect = true;
                    flag = 1;
                case 'trunk'
                    possible_intersect = true;
                    flag = 1; 
                case 'track'
                    possible_intersect = true;
                    flag = 1;                     
                case 'footway'
                    %possible_intersect = true;
                    flag = 0;
                case 'path'
                    %possible_intersect = true;
                    flag = 0;                 
                case 'pedestrian'
                    flag = 0;
                case 'steps'
                    flag = 0;
                case 'cycleway'
                    flag = 0;
                otherwise                    
                    if sum(ismember(key, 'name')) ~= 0
                        possible_intersect = true;
                        flag = 1;
                        temp_tags{end+1} = v;
                    elseif sum(ismember(key, 'maxspeed')) ~= 0
                        possible_intersect = true;
                        flag = 1;
                        temp_tags{end+1} = v;
                    else
                        flag = 0;
                    end
            end            
            
        case 'amenity'
            % bus station ?
            if strcmp(v, 'bus_station')
                disp('Bus station found')
            end
            if strcmp(v, 'parking')
                disp('Parking found')
                flag = 3;
            end
        case 'building' %[Wei-Chiu] some key value are building:xxx, need to be considered too
            flag = 2;
        case 'railway'
            flag = 4;
        case 'addr:city'
            flag = 2;
        case 'bicycle'
            %flag = 1;
        case 'maxspeed'
            flag = 1;
        case 'leisure'
            if strcmp(v, 'garden')
                flag = 5;
            end
        case 'landuse'
            if strcmp(v, 'grass') %|| strcmp(v, 'cemetery')
                flag = 5;            
            end            
        otherwise
            %disp('way without tag.')
    end
    
    % plot highway
    % [Wei-Chiu] Detect potential intersections
    way_nd_ids = way.nd{1, i};
    num_nd = size(way_nd_ids, 2);
    nd_coor = zeros(2, num_nd);
    nd_coor_m = zeros(2, num_nd);
    nd_ids = node.id;
            
    for j=1:num_nd
        cur_nd_id = way_nd_ids(1, j);
        if ~isempty(node.xy(:, cur_nd_id == nd_ids))
            nd_coor(:, j) = node.xy(:, cur_nd_id == nd_ids); %[Wei-Chiu] x, y: longitude, latitude
            if strcmp(version, 'meters')
                [nd_coor_m(1, j), nd_coor_m(2, j)] = calPos(nd_coor(:, j), origin_pos);
            end
            
            if possible_intersect == true
                if ~nd2coord.isKey(cur_nd_id)
                    nd2coord(cur_nd_id) = nd_coor(:, j);
                    nd2coord_m(cur_nd_id) = nd_coor_m(:, j);
                end
                
                if nd2way.isKey(cur_nd_id)
                    ori = nd2way(cur_nd_id);
                    ori(1, end+1) = way.id(1, i);
                    nd2way(cur_nd_id) = ori;
                else
                    nd2way(cur_nd_id) = [way.id(1, i)];
                end                
            end
        else
            error('cannot find this node');
        end
    end
    
    % remove zeros
    %nd_coor(any(nd_coor==0,2),:)=[];
    
    nd_coor(:, any(nd_coor==0,1))=[]; % [Wei-Chiu]
    nd_coor_m(:, any(nd_coor==0,1))=[]; % [Wei-Chiu]
    
    % [Wei-Chiu] Rough way to determine way direction    
    for j = 1:num_nd-1
        if possible_intersect == true
            dir = calRoadDirection(nd_coor(:, j), nd_coor(:, j+1), latitide_scale_factor, version);
            cur_nd_id = way_nd_ids(1, j);
            
            if strcmp(version, 'meters')
                plot(nd_coor_m(1, j), nd_coor_m(2, j), 'x')
            else
                plot(nd_coor(1, j), nd_coor(2, j), 'x')
            end
            
            if nd2direction.isKey(cur_nd_id)
                ori = nd2direction(cur_nd_id);
                ori(1, end+1) = dir;
                nd2direction(cur_nd_id) = ori;
                ori = nd_way_direction(cur_nd_id);
                if way_oneway(way.id(1, i)) == 1
                    ori(1, end+1) = 1;
                else
                    ori(1, end+1) = 2;
                end
                nd_way_direction(cur_nd_id) = ori;                
            else
                nd2direction(cur_nd_id) = [dir];
                if way_oneway(way.id(1, i)) == 1
                    nd_way_direction(cur_nd_id) = [1];
                else
                    nd_way_direction(cur_nd_id) = [2];
                end                
                
            end
            dir = calRoadDirection(nd_coor(:, j+1), nd_coor(:, j), latitide_scale_factor, version);
            
            cur_nd_id = way_nd_ids(1, j+1);
            if nd2direction.isKey(cur_nd_id)
                ori = nd2direction(cur_nd_id);
                ori(1, end+1) = dir;
                nd2direction(cur_nd_id) = ori;
                ori = nd_way_direction(cur_nd_id);
                if way_oneway(way.id(1, i)) == 1
                    ori(1, end+1) = 0;
                else
                    ori(1, end+1) = 2;
                end
                nd_way_direction(cur_nd_id) = ori;                   
            else
                nd2direction(cur_nd_id) = [dir];
                if way_oneway(way.id(1, i)) == 1
                    nd_way_direction(cur_nd_id) = [0];
                else
                    nd_way_direction(cur_nd_id) = [2];
                end                 
            end        
        end
    end
    
%     xy_diff_1 = nd_coor(:, 2) - nd_coor(:, 1);
%     xy_diff_2 = nd_coor(:, end) - nd_coor(:, end-1);        
%     way_length_1 = sqrt(sum(xy_diff_1 .^ 2, 1));
%     way_length_2 = sqrt(sum(xy_diff_2 .^ 2, 1));
%     direction = acosd(xy_diff(1, 1) / way_length);

    
    if strcmp(version, 'meters')
        show_coor = nd_coor_m;
    else
        show_coor = nd_coor;
    end

    if ~isempty(show_coor)
        % plot way (highway = blue, other = green)
        if flag == 1
            plot(hax, show_coor(1,:), show_coor(2,:), 'b-')
            %text(nd_coor(1, round(num_nd/2)), nd_coor(2, round(num_nd/2)), num2str(direction));
            %text(nd_coor(1, 1), nd_coor(2, 1), num2str(dir_1));
            %text(nd_coor(1, end), nd_coor(2, end), num2str(dir_2));
            road_out{road_ind} = nd_coor;
            road_ind = road_ind + 1;
        elseif flag == 2
            building_out{building_ind} = nd_coor;
            building_ind = building_ind + 1;
            plot(hax, show_coor(1,:), show_coor(2,:), 'r-')
        elseif flag == 3
            parking_out{parking_ind} = nd_coor;
            parking_ind = parking_ind + 1;
            plot(hax, show_coor(1,:), show_coor(2,:), 'k-')
        elseif flag == 4
            plot(hax, show_coor(1,:), show_coor(2,:), 'm-')
        elseif flag == 5
            fill(show_coor(1,:), show_coor(2,:), 'g-', 'edgecolor', 'c'); 
            %plot(hax, show_coor(1,:), show_coor(2,:), 'y-')
        else
            %plot(hax, nd_coor(1,:), nd_coor(2,:), 'g--')
        end
    end
    
    %waitforbuttonpress
end

[nd2direction, nd_way_direction] = sort_nd2direction(nd2direction, nd_way_direction);

% [Wei-Chiu]
count = 0;
if isempty(nd2way) == 0
    inter = nd2way.keys;
    for i = 1:size(inter,2)
        if size(nd2way(inter{1, i}), 2) > 1
            [inter_class, inter_type] = classifyIntersection(nd2direction(inter{1, i}));
            if inter_class == 0
                continue
            end
            nd_coor = nd2coord(inter{1, i});
            nd_coor_m = nd2coord_m(inter{1, i});
            intersection_out{intersection_ind} = nd_coor;
            intersection_ind = intersection_ind + 1;
            if strcmp(version, 'meters')
                x = nd_coor_m(1,:); 
                y = nd_coor_m(2,:);                
            else              
                x = nd_coor(1,:); 
                y = nd_coor(2,:);
            end
            plot(x, y, 'o', 'MarkerFaceColor', 'k', 'MarkerSize', 6);
            inter_matrix = [inter_matrix; inter{1, i} nd_coor' nd_coor_m' inter_class, inter_type];
%             directions = nd2direction(inter{1, i});
%             for in = 1:size(directions, 2)
%                 r = 0.00007;
%                 t = directions(1, in);
%                 plot([x x+r*cosd(t)], [y y+r*sind(t)*latitide_scale_factor], 'g-');
%                 %text(x+r*cos(t), y+r*sin(t)*latitide_scale_factor, num2str(rad2deg(t)));
%             end
            count = count + 1;
        end
    end
end

[inter_matrix, nd2direction] = mergeIntersection(inter_matrix, nd2direction, nd_way_direction, nd2way, latitide_scale_factor, version);

disp(key_catalog.')

function [] = disp_info(bounds, Nnode, Nway)
disp( ['Bounds: xmin = ' num2str(bounds(1,1)),...
    ', xmax = ', num2str(bounds(1,2)),...
    ', ymin = ', num2str(bounds(2,1)),...
    ', ymax = ', num2str(bounds(2,2)) ] )
disp( ['Number of nodes: ' num2str(Nnode)] )
disp( ['Number of ways: ' num2str(Nway)] )


function [inter_class, inter_type] = classifyIntersection(directions)

ind = directions < 0;
directions(ind) = directions(ind) + 360;
[sort_dir, ind] = sort(directions);
dist = diff([sort_dir; sort_dir(1, 2:end) sort_dir(1, 1)+360]);

inter_class = 1;
inter_type = zeros(1, 3);

road_num = size(dist, 2);
switch (road_num)
    case 2
        inter_class = 0;
%         if abs(diff(dist)) < 90
%             inter_class = 0;            
%         else
%             inter_class = 2;
%         end
    case 3
        inter_class = 3;
        [m, i] = max(dist);
        rotate_dist = [m];
        ind_ord = [i];
        for k = 1:2
            i = mod(i+1, 3);
            if i == 0
                i = 3;
                rotate_dist = [rotate_dist, dist(1, i)];
            else
                rotate_dist = [rotate_dist, dist(1, i)];
            end
            ind_ord = [ind_ord, i];
        end
        %[sort_dist , ind] = sort(dist);
        t_type = inDeviation([180 90 90], rotate_dist, 20);
        if t_type
            for k = 1:3
                inter_type(ind(ind_ord(k))) = k;
            end
        end        
    case 4
        inter_class = 4;
    otherwise
end
%dist = pdist2(directions', directions', 'cityblock');
% dist = triu(dist, 1);
% ind = (dist ~= 0);
% dist = dist(ind);
% dist = sort(mod(dist, 180));
% 
% inter_class = 1;
% inter_type = 1;
% switch size(dist, 1)
%     case 1
%         if dist > 135 || dist < 45
%             inter_class = 0;
%         end
%     case 3
%         inter_class = 3;
%     case 6
%         inter_class = 4;
%     otherwise
% end


function [same] = inDeviation(gt, inter, theta)
qualified = (inter < gt + theta) & (inter > gt - theta); 

if sum(qualified) == 3
    same = 1;
else
    same = 0;
end

function [inter_matrix, nd2direction] = mergeIntersection(inter_matrix, nd2direction, nd_way_direction, nd2way, latitude_scale_factor, version)
    THREDHOLD = 20;
    inter_num = size(inter_matrix, 1);
    m_inter_matrix = [];
    for i = 1:inter_num
        if inter_matrix(i, 6) ~= 3
            continue
        end
        x0 = inter_matrix(i, 4);
        y0 = inter_matrix(i, 5);
        cand = (inter_matrix(:, 6) == 4) & (inter_matrix(:, 4) > x0 - THREDHOLD) & (inter_matrix(:, 4) < x0 + THREDHOLD) & ...
            (inter_matrix(:, 5) > y0 - THREDHOLD) & (inter_matrix(:, 5) < y0 + THREDHOLD);     
        cand = inter_matrix(cand, :);
        for j = 1:size(cand, 1)
            same = onSameRoad(inter_matrix(i, 1), cand(j, 1), nd2way);
            if same
                dir = nd2direction(inter_matrix(i, 1));
                fin = calRoadDirection(inter_matrix(i, 2:3)', cand(j, 2:3)', latitude_scale_factor, version, 'r');
                d = findClosestDirection(fin, deg2rad(dir));
                if inter_matrix(i, 6+d) == 3
                    m_inter_matrix = [m_inter_matrix; cand(j, 1) inter_matrix(i, :)];
                    break;
                end
            end
        end        
    end
    for i = 1:size(m_inter_matrix, 1)
        ind = (inter_matrix(:, 1) == m_inter_matrix(i, 1));
        inter_matrix(ind, end-3:end) = m_inter_matrix(i, end-3:end);
        nd2direction(m_inter_matrix(i, 1)) = nd2direction(m_inter_matrix(i, 2));
        nd_way_direction(m_inter_matrix(i, 1)) = nd_way_direction(m_inter_matrix(i, 2));
    end
    
    
    
%     for i = 1:inter_num
%         x0 = inter_matrix(i, 4);
%         y0 = inter_matrix(i, 5);
%         cand = (inter_matrix(:, 4) > x0 - THREDHOLD) & (inter_matrix(:, 4) < x0 + THREDHOLD) & ...
%             (inter_matrix(:, 5) > y0 - THREDHOLD) & (inter_matrix(:, 5) < y0 + THREDHOLD);        
%     end

function [same] = onSameRoad(inter1, inter2, nd2way)
    same = false;
    cand1 = nd2way(inter1);
    cand2 = nd2way(inter2);
    for i = 1:size(cand1, 2)
        if sum(cand2 == cand1(1, i)) ~= 0
            same = true;
            break
        end
    end

function [nd2direction, nd_way_direction] = sort_nd2direction(nd2direction, nd_way_direction)
    inter_num = length(nd2direction);
    inter_key = nd2direction.keys;
    for i = 1:inter_num
        dir = nd2direction(inter_key{1, i});
        ind = dir < 0;
        dir(ind) = dir(ind) + 360;
        [sort_dir, ind] = unique(dir);
        nd2direction(inter_key{1, i}) = sort_dir;
        
        wdir = nd_way_direction(inter_key{1, i});
        nd_way_direction(inter_key{1, i}) = wdir(ind);
    end



