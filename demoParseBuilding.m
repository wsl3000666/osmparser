clc;clear;
OSM_DIR = '/ais/gobi3/u/slwang/insideout';
BUILDING_DIR = '/ais/gobi3/u/slwang/insideout/building'; mkdir(BUILDING_DIR);

osm_list = dir(fullfile(OSM_DIR, '*.osm'));

for i = 1 : length(osm_list)
    osm_filename = fullfile(OSM_DIR, sprintf('%04d.osm', i));
    % osm_filename = fullfile(OSM_DIR, 'small_map.osm');
    fprintf('%04d: ', i-1);
    tic;
    [parsed_osm, osm_xml] = parse_openstreetmap(osm_filename);
    fig = figure(1);
    ax = axes('Parent', fig);
    hold(ax, 'on')
    buildings = plot_way(ax, parsed_osm);
    toc
    building_filename = fullfile(BUILDING_DIR, sprintf('%04d.mat', i));
    save(building_filename, 'buildings');
    pause(0.01);
end