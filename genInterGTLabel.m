%function [] = genInterGTLabel()

OSM_DIR = 'data/map75';
PROJ_DIR = 'data/proj';
META_DIR = '/ais/gobi3/u/slwang/kitti3d/code/sun_dir/data';
VERSION = 'meters';

osm_list = dir(fullfile(OSM_DIR, '*.osm'));
meta_list = dir(fullfile(META_DIR, '*.mat'));

train_osm_list = osm_list(1:end);
%test_osm_list = osm_list(end-6:end);
train_meta_list = meta_list(1:end);
%test_meta_list = meta_list(end-6:end);

train_label_roadnum = [];
train_label_roadtype = [];
train_cnn_label = [];
train_dist2inter = [];
train_imgs = [];
train_seqs = [];
test_label_roadnum = [];
test_label_roadtype = [];

temp_tags = [];

k = 1;
for i = 1:length(train_osm_list)
    file_name = fullfile(OSM_DIR, train_osm_list(i, 1).name);
    [~, name, ~] = fileparts(file_name);
    
    fprintf('parsing %s...', train_osm_list(i, 1).name);
    if ~exist(fullfile(OSM_DIR, [name '.mat']))
        [parsed_osm, osm_xml] = parse_openstreetmap(file_name);    
        save(fullfile(OSM_DIR, [name '.mat']), 'parsed_osm');
    else
        load(fullfile(OSM_DIR, [name '.mat']));
    end
    fprintf('done. ');
    
    fig = figure(1);
    clf(fig);
    ax = axes('Parent', fig);
    hold(ax, 'on')
    [buildings, roads, parkings, intersections, inter_matrix, bounds, nd2direction, nd_way_direction, origin_pos, temp_tags] = plot_way_slwang(ax, parsed_osm, VERSION, temp_tags); 
    
    load(fullfile(META_DIR, train_meta_list(i, 1).name));
    
    seqs = [];
    qualified = [];
    last_frame = [];
    for j = 1:5:length(metadata)
        if j == 1
            seqs = [seqs; metadata(1, j).oxts(1, [1 2 6])];
            qualified = [qualified; j];
            last_frame = metadata(1, j).oxts(1, [1 2 6]);
        else
            similar = checkTooClose(last_frame, metadata(1, j).oxts(1, [1 2 6]));
            if ~similar
                seqs = [seqs; metadata(1, j).oxts(1, [1 2 6])];
                qualified = [qualified; j];
                last_frame = metadata(1, j).oxts(1, [1 2 6]);
            end
        end
    end
    
    fprintf('prjecting sequence to map...');
    [inter_label, inter_type, inter_dist] = projSeq2Map({seqs}, 1, inter_matrix, nd2direction, nd_way_direction, VERSION, origin_pos, 1, k, PROJ_DIR);
    cnn_inter_label = mapInterType2to1(inter_label, inter_type);
    %saveas(fig, fullfile(OSM_DIR, [name '.png']));
    fprintf('done. ');
    
    fprintf('generating blend picture...');
    ind = 1;
    for j = 1:size(qualified, 1)
        train_label_roadnum{k} = inter_label(ind, 1);
        train_label_roadtype{k} = inter_type(ind, 1);
        train_cnn_label{k} = cnn_inter_label(ind, 1);
        train_dist2inter{k} = inter_dist(ind, 1);
        train_imgs{k} = metadata(1, qualified(j)).name;
        train_seqs{k} = i;                
        
%         im1 = imread(metadata(1, qualified(j)).name);
%         im2 = imread(fullfile(PROJ_DIR, sprintf('%06d.png', k))); 
%         im1 = im2double(im1);
%         im2 = im2double(im2);
%         im1 = imresize(im1, [375 1242]);
%         im2 = imresize(im2, 1242/size(im2, 2));
%         big = zeros(size(im2, 1)+375, 1242, 3);
%         big(1:375, :, :) = im1;
%         big(376:end, :, :) = im2;
%         
%         imwrite(big, fullfile(PROJ_DIR, sprintf('blend_%06d_%06d.png', k, i)));
        
        k = k + 1;
        ind = ind + 1;
    end
    fprintf('done.\n');        

    hold off
    close all

    save data_inter train_cnn_label train_imgs train_seqs train_label_roadnum train_label_roadtype train_dist2inter
end

save('temp_tags.mat', 'temp_tags');
% k = 1;
% for i = 1:length(test_osm_list)
%     file_name = fullfile(OSM_DIR, test_osm_list(i, 1).name);
%     [~, name, ~] = fileparts(file_name);
%     
%     fprintf('parsing %s...\n', test_osm_list(i, 1).name);
%     if ~exist(fullfile(OSM_DIR, [name '.mat']))
%         [parsed_osm, osm_xml] = parse_openstreetmap(file_name);    
%         save(fullfile(OSM_DIR, [name '.mat']), 'parsed_osm');
%     else
%         load(fullfile(OSM_DIR, [name '.mat']));
%     end
%     fprintf('done. ');    
%     
%     fig = figure(1);
%     clf(fig);
%     ax = axes('Parent', fig);
%     hold(ax, 'on')
%     [buildings, roads, parkings, intersections, inter_matrix, bounds, nd2direction, nd_way_direction, origin_pos] = plot_way_slwang(ax, parsed_osm, 'VERSION'); 
%     
%     load(fullfile(META_DIR, test_meta_list(i, 1).name));
%     
%     seqs = [];
%     for j = 1:10:length(metadata)
%         seqs = [seqs; metadata(1, j).oxts(1, [1 2 6])];
%     end
%     
%     fprintf('prjecting sequence to map...');
%     [inter_label, inter_type] = projSeq2Map(seqs, inter_matrix, nd2direction, nd_way_direction, VERSION, origin_pos, 1, k, PROJ_DIR);
%     %saveas(fig, fullfile(OSM_DIR, [name '.png']));
%     fprintf('done. ');
%     
%     fprintf('generating blend picture...');
%     ind = 1;
%     for j = 1:10:length(metadata)
%         test_label_roadnum{k} = inter_label(ind, 1);
%         test_label_roadtype{k} = inter_type(ind, 1);
%                 
%         im1 = imread(metadata(1, j).name);
%         im2 = imread(fullfile(PROJ_DIR, sprintf('%06d.png', k)));
%         im1 = im2double(im1);
%         im2 = im2double(im2);
%         im1 = imresize(im1, [375 1242]);
%         im2 = imresize(im2, 1242/size(im2, 2));
%         big = zeros(size(im2, 1)+375, 1242, 3);
%         big(1:375, :, :) = im1;
%         big(376:end, :, :) = im2;
%         
%         imwrite(big, fullfile(PROJ_DIR, sprintf('blend_%06d.png', k)));        
%         
%         k = k + 1;
%         ind = ind + 1;
%     end
%     fprintf('done.\n');
%     
%     hold off
%     close all
% end

%end

