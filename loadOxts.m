function oxts = loadOxts(base_dir)

file_list = dir([base_dir '/*.txt']);
file_num = size(file_list, 1);

oxts = cell(file_num, 1);
for i = 1:file_num
    fname = file_list(i, 1).name;
    oxts{i} = dlmread(fullfile(base_dir, fname));
end

end