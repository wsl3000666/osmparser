lat_val = lat(idx);
lon_val = lon(idx);
apt_list_val = apt_list(idx);
f_osm = fopen('getLayoutOSM.sh', 'wt+');
for i = 1:length(lat_val)
    lt_lat = lat_val(i) - 0.0005;
    lt_lon = lon_val(i) - 0.001;
    rb_lat = lat_val(i) + 0.0005;
    rb_lon = lon_val(i) + 0.001;
    fprintf(f_osm, 'wget -O ./osm/%s.osm http://api.openstreetmap.org/api/0.6/map?bbox=%2.12f,%2.12f,%2.12f,%2.12f\n', apt_list_val(i).name, lt_lon, lt_lat, rb_lon, rb_lat);
    fprintf('processing %s...\n', apt_list_val(i).name);
end
fclose(f_osm);
