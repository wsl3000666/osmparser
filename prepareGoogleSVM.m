function [] = prepareGoogleSVM()

% Meta variables
TRAINING_FILE = '/u/weichium/caffe/data/vgg_inter/test.txt';
TESTING_FILE = '/u/weichium/caffe/data/vgg_inter/test.txt';
MEAN_DATA = '/u/slwang/Codes/caffe/data/kittisun/kittisun_mean.binaryproto';
OUTPUT_DIR = '/u/weichium/caffe/data/google_inter/prediction';

% Add caffe/matlab to you Matlab search PATH to use matcaffe
addpath '/u/weichium/caffe/matlab'

% Set caffe mode
caffe.set_mode_gpu();
gpu_id = 0;  % we will use the first gpu in this demo
caffe.set_device(gpu_id);

% Initialize the network using BVLC CaffeNet for image classification
% Weights (parameter) file needs to be downloaded from Model Zoo.
model_dir = '/u/weichium/caffe/models/google_inter/solver/';
net_model = [model_dir 'deploy_loss2.prototxt'];
net_weights = [model_dir 'google_inter_iter_5000.caffemodel'];
phase = 'test'; % run with phase test (so that dropout isn't applied)

if ~exist(net_weights, 'file')
    error('Please download CaffeNet from Model Zoo before you run this demo');
end

% Initialize a network
net = caffe.Net(net_model, net_weights, phase);
mean_data = caffe.io.read_mean(MEAN_DATA);

f_test = fopen(TESTING_FILE);
C = textscan(f_test, '%s %d');
fclose(f_test);

img_num = size(C{1, 1}, 1);
img_list = C{1, 1};
gt_label = C{1, 2};

svm_test_data = zeros(img_num, feature_num);
svm_test_label = zeros(img_num, 1);

for i = 1:img_num
    fprintf('predicting test image %06d.jpg', i);
    im = prepare_image(img_list{i, 1}, mean_data);
    scores = net.forward({im});
    
     %= net.blobs('').get_data();
    
end

end

function [data] = prepare_image(img_path, mean_data)

IMAGE_DIM = 256;
CROPPED_DIM = 224;

im = caffe.io.load_image(img_path);
im = imresize(im, [IMAGE_DIM, IMAGE_DIM], 'bilinear'); % resize using Matlab's imresize
im = im - mean_data;
im = imresize(im, [CROPPED_DIM, CROPPED_DIM], 'bilinear');

data = zeros(CROPPED_DIM, CROPPED_DIM, 3, 1, 'single');
data(:, :, :, 1) = im;

end